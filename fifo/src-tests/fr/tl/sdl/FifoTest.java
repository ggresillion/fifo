package fr.tl.sdl;

import fr.tl.sdl.Fifo;
import fr.tl.sdl.FifoHead;
import fr.tl.sdl.FifoQueue;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * JUnit test for fr.tl.sdl.Fifo
 * 
 * @version $Id: FifoTest.java,v 1.1 2005/02/17 09:31:34 tombelle Exp $
 * @author Christophe TOMBELLE
 */
public class FifoTest extends TestCase {
	/** Objects used by the test */
	private Object o3, o4, o5;
	/** Objects used by the test */
	private Object i4, i5;

	/**
	 * Build a FifoTest instance with the specified name.
	 * 
	 * @param testName
	 *            Name for the test.
	 */
	public FifoTest(java.lang.String testName) {
		super(testName);
	}

	/**
	 * Build and get a test suite for this class.
	 * 
	 * @return Test suite for this class.
	 */
	public static Test suite() {
		TestSuite suite = new TestSuite(FifoTest.class);
		return suite;
	}

	/**
	 * Builds obects needed for testing purpose.
	 */
	public void setUp() {
		o3 = new Double(3);
		o4 = new Double(4);
		o5 = new Double(5);
		i4 = new Integer(4);
		i5 = new Integer(5);
	}

	/**
	 * Deletes objects needed for testing purpose.
	 */
	public void tearDown() {
		o3 = null;
		o4 = null;
		o5 = null;
		i4 = null;
		i5 = null;
	}

	/**
	 * Test Fifo constructor.
	 */
	public void testFifo() {
		try {
			Fifo fifo = new fr.tl.sdl.Fifo();
			FifoHead fifoHead = fifo;
			FifoQueue fifoQueue = fifo;
			assertEquals(0, fifoHead.getSize());
			assertNull(fifoHead.getHead());
			fifoHead.remove(); // should cause no exception
			fifoHead.save(Double.class); // idem
			fifoHead.save(4); // idem
			fifoQueue.add(null); // shouldn't modify fifo state
			assertEquals(0, fifoHead.getSize());
		} catch (Exception e) {
			fail("testFifo : " + e);
		}
	}

	/**
	 * Test basic behaviour for Object signals
	 */
	public void testSignals() {
		try {
			Fifo fifo = new fr.tl.sdl.Fifo();
			FifoHead fifoHead = fifo;
			FifoQueue fifoQueue = fifo;
			fifoQueue.add(o5);
			assertEquals(1, fifoHead.getSize());
			assertSame(o5, fifoHead.getHead());
			fifoQueue.add(o4);
			assertEquals(2, fifoHead.getSize());
			assertSame(o5, fifoHead.getHead());
			fifoQueue.add(o3);
			assertEquals(3, fifoHead.getSize());
			assertSame(o5, fifoHead.getHead());
			// remove 5
			fifoHead.remove();
			assertEquals(2, fifoHead.getSize());
			assertSame(o4, fifoHead.getHead());
			// remove 4
			fifoHead.remove();
			assertEquals(1, fifoHead.getSize());
			assertSame(o3, fifoHead.getHead());
			// remove 3
			fifoHead.remove();
			assertEquals(0, fifoHead.getSize());
			assertNull(fifoHead.getHead());
			fifoHead.remove();
		} catch (Exception e) {
			fail(e.toString());
		}
	}

	/**
	 * Test basic behaviour for int signals.
	 */
	public void testInt() {
		try {
			Fifo fifo = new fr.tl.sdl.Fifo();
			FifoHead fifoHead = fifo;
			FifoQueue fifoQueue = fifo;
			fifoQueue.add(5);
			assertEquals(1, fifoHead.getSize());
			assertEquals(i5, fifoHead.getHead());
			fifoHead.remove();
			fifoHead.remove();
		} catch (Exception e) {
			fail(e.toString());
		}
	}

	/**
	 * Test save feature.
	 */
	public void testSave() {
		try {
			// initialize the fifo
			Fifo fifo = new fr.tl.sdl.Fifo();
			FifoHead fifoHead = fifo;
			FifoQueue fifoQueue = fifo;
			fifoHead.save(Double.class);
			fifoQueue.add(o3);
			fifoQueue.add(o3);
			fifoQueue.add(o3);
			fifoQueue.add(4);
			fifoQueue.add(5);
			fifoQueue.add(5);
			// save( Class )
			fifoHead.save(Double.class);
			assertEquals(3, fifoHead.getSize());
			assertEquals(i4, fifoHead.getHead());
			// remove 4
			fifoHead.remove();
			assertEquals(i5, fifoHead.getHead());
			// save( int )
			fifoHead.save(5);
			assertEquals(0, fifoHead.getSize());
			// reset savings
			fifoHead.save(null);
			assertEquals(5, fifoHead.getSize());
			// remove and check remaining signals
			fifoHead.remove();
			assertEquals(4, fifoHead.getSize());
			fifoHead.remove();
			assertEquals(3, fifoHead.getSize());
			fifoHead.remove();
			assertEquals(2, fifoHead.getSize());
			fifoHead.remove();
			assertEquals(1, fifoHead.getSize());
			fifoHead.remove();
			assertEquals(0, fifoHead.getSize());
			fifoHead.remove();
			assertEquals(0, fifoHead.getSize());
			assertNull(null, fifoHead.getHead());
		} catch (Exception e) {
			fail(e.toString());
		}
	}

	/**
	 * This test case has a main so that it can be launched as a standalone
	 * application.
	 * 
	 * @param straArgs
	 *            The command line arguments (unused).
	 */
	public static void main(String[] straArgs) {
		junit.textui.TestRunner.run(suite());
	}
}
