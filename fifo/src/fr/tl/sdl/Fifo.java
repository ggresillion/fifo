package fr.tl.sdl;

import java.util.ArrayList;

public class Fifo implements FifoHead, FifoQueue {

	ArrayList<Object> list = new ArrayList<Object>();
	ArrayList<Object> saved = new ArrayList<Object>();

	@Override
	public void add(Object oSig) {
		if (!(oSig == null)) {
			list.add(oSig);
		}

	}

	@Override
	public void add(int iKind) {
		list.add(iKind);
	}

	@Override
	public int getSize() {
		return list.size();
	}

	@Override
	public void remove() {
		if (list.size() != 0) {
			list.remove(0);
		}
	}

	@Override
	public Object getHead() {
		if (list.size() != 0) {
			return list.get(0);
		}
		return null;
	}

	@Override
	public void save(Class clsSig) {
		if(clsSig==null){
			for(Object o :saved){
				list.add(o);
			}
		}
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getClass().equals(clsSig)) {
				saved.add(list.get(i));
				list.remove(i);
				i--;
			}
		}

	}

	@Override
	public void save(int iSig) {
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getClass().equals(Integer.class)) {
				saved.add(list.get(i));
				list.remove(i);
				i--;
			}
		}
	}

}
